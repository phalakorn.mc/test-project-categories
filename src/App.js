import { Input } from 'antd';
import { useState, useEffect} from "react";
import axios from 'axios';
import 'antd/dist/antd.min.css';
import './App.scss';

function App() {
  const [data,setData] = useState();
  const [search,setSearch] = useState('');
  const onChange = (e) =>{
    setSearch(e.target.value);
  }

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `https://api.publicapis.org/categories`,
      );
      setData(result.data.categories);
    };

    fetchData();
  }, []);

  async function thisIsSyncFunction() {
    let result = 0;
    return await fetch('https://api.publicapis.org/categories').then(res => res.json()).then((response) =>
    {
      result = response.count;
      
      const number1 = result;
      const calculation = number1 * 10;
      console.log(calculation);
    });
    }
    thisIsSyncFunction();
    

  return (
    <div className="App">
      <div className='block-search'>
        <label>search :</label>
        <Input onChange={(e) => onChange(e)} />
      </div>
      <table>
        <tr>
          <th>Categories</th>
        </tr>
        {data?.filter(name => name.toLowerCase().includes(search.toLowerCase())).map(category => (
          <tr>
            <td>{category}</td>
          </tr>
        ))}
      </table>
    </div>
  );
}

export default App;
